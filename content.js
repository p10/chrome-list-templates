var keywords = ['Render', 'Decorate content with'];

function indexOf(array, item) {
  for (var i = 0, len = array.length; i < len; ++i) {
    if (array[i] === item) {
      return i;
    }
  }
  return -1;
}

function include(array, item) {
  if (indexOf(array, item) == -1) {
    array.push(item);
  }
}

function getLog(td) {

  var
    txt = td.innerHTML.replace(/<(.|\n)*?>/g, ''),
    regex = new RegExp('^('+ keywords.join('|') +') \"(.+?)\"', ''),
    match = txt.match(regex);

  return match && formatLog(match[1], match[2]);
}

function getLogs() {

  var td, log, logs = [],
      container = document.getElementById('sfWebDebugLogLines'),
      rows;
      
  if (container) {
    rows = container.getElementsByTagName('td');
    for (var i = 0, l = rows.length; i < l; ++i) {
      td = rows[i];
      if (td.className == '') {
        log = getLog(td);
        if (log) {
           include(logs, log);
        }
      }
    }
  }

  return logs;
}

function formatLog(title, path) {
  return title.replace(keywords[0], 'R').replace(keywords[1], 'D') + ' ' + path.replace('sf_app_dir/', '');
}


chrome.extension.onRequest.addListener(function(request, sender, callback) {
  if (request.type === 'location') {
    var controller = request.controller;
    if (location.pathname.indexOf(controller) != -1) {
      location.href = location.href.replace(controller, '');
    } else {
      location.pathname = controller.concat(location.pathname);
    }
  } else if (request.type === 'templates') {
    callback({
      templates: getLogs()
    });
  }
});


chrome.extension.sendRequest({id: 'templates', templates: getLogs()});