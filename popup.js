var renderTemplates = function(templates) {
  var container, ul, li;
    
  if (templates.length) {
    ul = document.createElement('ul');
    templates.forEach(function(template, i) {
      li = document.createElement('li');
      li.innerHTML = template;
      ul.appendChild(li);
    });
    container = document.getElementById('templates');
    container.innerHTML = '';
    container.appendChild(ul);
    container.style.display = 'block';
  }
};

var getTemplates = function() {
  chrome.tabs.getSelected(null, function(tab) {
    chrome.tabs.sendRequest(tab.id, {type: 'templates'}, function(response) {
      renderTemplates(response.templates);
    });
  });
};


Array.prototype.forEach.call(document.querySelectorAll('button.devmode'), function(button) {
  button.addEventListener('click', function(ev) {
    chrome.tabs.getSelected(null, function(tab) {
      chrome.tabs.sendRequest(tab.id, {
        type: "location",
        controller: '/'+button.getAttribute('data-controller')
      });
    });
  }, false);
});

chrome.extension.onRequest.addListener(function(request, sender, callback) {
  if (request.id === 'templates') {
    renderTemplates(request.templates);
  }
});

getTemplates();